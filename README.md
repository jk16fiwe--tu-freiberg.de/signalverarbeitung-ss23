# Signalverarbeitung SS22
[![Binder][binder-logo]][binder-url] [![CC BY 4.0][cc-by-shield]][cc-by]

Unterlagen zum Praktikum der Lehrveranstaltung "Signalverarbeitung" an der TU Freiberg im Sommersemester 2023. 

Informationen zur Lehrveranstaltung finden Sie im [OPAL](https://bildungsportal.sachsen.de/opal/auth/RepositoryEntry/34357116933).

## Praktikumsaufgaben

* [Praktikum 1](/P-1/SigProc_Praktikum01.ipynb)

## Lizenz

Diese Arbeit unterliegt den Bestimmungen einer
[Creative Commons Namensnennung 4.0 International-Lizenz][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: https://creativecommons.org/licenses/by/4.0/deed.de
[cc-by-image]: https://licensebuttons.net/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
[binder-logo]: https://mybinder.org/badge_logo.svg
[binder-url]: https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.hrz.tu-chemnitz.de%2Fjs68viga--tu-freiberg.de%2Fsignalverarbeitung-ss23/HEAD

